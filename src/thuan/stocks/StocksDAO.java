/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thuan.stocks;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import thuan.utils.DBUtils;

/**
 *
 * @author thuan
 */
public class StocksDAO {

    Connection conn = null;
    PreparedStatement stm = null;
    ResultSet rs = null;

    public void closeConnection(){
        try {
        if (rs != null) {            
                rs.close();
            
        }
        if (stm != null) {
            stm.close();
        }
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException ex) {
                Logger.getLogger(StocksDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    public boolean inserDataTrans(ArrayList<StocksDTO> list) throws  ClassNotFoundException, SQLException {
        boolean result = false;
        try {
            conn = DBUtils.getConnection();
            conn.setAutoCommit(false);
            if (conn != null) {
                String sql = "INSERT INTO Stocks VALUES ( ?, ?, ?, ?, ?)";
                stm = conn.prepareStatement(sql);
                for (StocksDTO dto : list) {
                    stm.setInt(1, dto.getStockID());
                    stm.setString(2, dto.getStockName());
                    stm.setString(3, dto.getAddress());
                    Date date = new Date(dto.getDateAvailble().getTime());
                    stm.setDate(4, date);
                    stm.setString(5, dto.getNote());
                    stm.executeUpdate();
                }
                conn.commit();
                result = true;
            }
        } catch (SQLException e) {
            conn.rollback();
        } finally {
            closeConnection();
        }
        return result;
    }

}
