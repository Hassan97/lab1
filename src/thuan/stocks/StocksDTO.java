/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thuan.stocks;

import java.util.Date;

/**
 *
 * @author thuan
 */
public class StocksDTO {
    private int stockID;
    private String stockName;
    private String address;
    private Date dateAvailble;
    private String note;

    public StocksDTO() {
    }

    public StocksDTO(int stockID, String stockName, String address, Date dateAvailble, String note) {
        this.stockID = stockID;
        this.stockName = stockName;
        this.address = address;
        this.dateAvailble = dateAvailble;
        this.note = note;
    }

    public int getStockID() {
        return stockID;
    }

    public void setStockID(int stockID) {
        this.stockID = stockID;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateAvailble() {
        return dateAvailble;
    }

    public void setDateAvailble(Date dateAvailble) {
        this.dateAvailble = dateAvailble;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
    
}
